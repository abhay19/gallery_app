package madocs.skapps.com.assignmentapplication.model

data class GalleryItem(val albumId:String,
                        val id: Int,
                        val title: String,
                        val url: String,
                        val thumbnailUrl: String)
